<?php

include_once plugin_dir_path(__FILE__).'timeline-widgets.php';

class Chronos_Timeline
{
    public function __construct()
    {
        add_action('widgets_init', function() {
            register_widget('Chronos_Timeline_Widget');
        });

        add_action('wp_loaded', array($this, 'add_timeline'));
        add_shortcode('chronos_timeline_frontend', array($this, 'chronos_timeline_frontend'));

        wp_register_style( 'timeline.css', plugin_dir_url( __FILE__ ) . 'assets/styles/timeline.css', array(), '0.0' );
        wp_register_style( 'chronos-form.css', plugin_dir_url( __FILE__ ) . 'assets/styles/chronos-form.css', array(), '0.0' );

        wp_enqueue_style( 'timeline.css');
        wp_enqueue_style( 'chronos-form.css');
    }

    public function chronos_timeline_frontend($atts, $content)
    {
        if(!empty($atts['order'])){
            $order = $atts['order'];
        } else {
            $order = "ASC";
        }

        global $wpdb;
        $table_name = $wpdb->prefix . "chronos_timeline";

        $rows = $wpdb->get_results("SELECT * from $table_name ORDER BY timeline_date $order");
        ?>
        <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/Chronos/functions/assets/styles/chronos-timeline-style.css" rel="stylesheet" />
        <div class="main-container">
            <section id="timeline" class="timeline-outer">
                <div class="container" id="content">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <ul class="timeline">
                                <?php foreach ($rows as $row) { ?>
                                    <li class="event" data-date="<?= $row->timeline_date ?>">
                                        <a href="<?= $row->link ?>" class="timeline-link">
                                            <img src="<?= $row->image ?>" class="timeline-image">
                                            <h3 class="timeline-title"><?= $row->title ?></h3>
                                            <p class="timeline-description">
                                                <?= $row->description ?>
                                            </p>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php
    }
}