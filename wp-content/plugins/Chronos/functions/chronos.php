<?php

class Chronos_Plugin
{
    public function __construct()
    {
        include_once plugin_dir_path( __FILE__ ).'chronos-timeline.php';

        add_action('admin_menu', array($this, 'add_admin_menu'));

        new Chronos_Timeline();
    }

    public function add_admin_menu()
    {
        //this is the main item for the menu
        add_menu_page('Chronos - Timeline', //page title
            'Chronos plugin', //menu title
            'manage_options', //capabilities
            'chronos_timeline_list', //menu slug
            array($this, 'chronos_timeline_list') //function
        );

        //this is a submenu
        add_submenu_page('chronos_timeline_list', //parent slug
            'Add Timeline', //page title
            'Add New', //menu title
            'manage_options', //capability
            'chronos_timeline_create', //menu slug
            array($this, 'chronos_timeline_create')
        ); //function

        //this submenu is HIDDEN, however, we need to add it anyways
        add_submenu_page(null, //parent slug
            'Update Timeline', //page title
            'Update', //menu title
            'manage_options', //capability
            'chronos_timeline_update', //menu slug
            array($this, 'chronos_timeline_update')
        ); //function
    }

    function chronos_timeline_create() {
        $message = null;
        $title = $_POST["timeline_title"];
        $image_link = null;
        $description = $_POST["timeline_description"];
        $link = $_POST["timeline_link"];
        $date = $_POST["timeline_date"];

        //insert
        if (isset($_POST['insert'])) {

            wp_upload_bits($_FILES['timeline_image']['name'], null, file_get_contents($_FILES['timeline_image']['tmp_name']));
            $image_link = '/wp-content/uploads/'.date("Y").'/'.date("m").'/'.str_replace(' ','-',$_FILES['timeline_image']['name']);

            global $wpdb;
            $table_name = $wpdb->prefix . "chronos_timeline";

            if(!empty($title) && !empty($image_link) && !empty($description) && !empty($link) && !empty($date)) {
                $wpdb->insert(
                    $table_name, //table
                    array(
                        'title' => $title,
                        'image' => $image_link,
                        'description' => $description,
                        'link' => $link,
                        'timeline_date' => $date
                    ), //data
                    array('%s', '%s') //data format
                );
                $message.="Timeline ajoutez";
            } else {
                $message.="Tous les champs doivent être complétez";
            }
            $wpdb->show_errors();
        }
        ?>
        <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/functions/chronos-form.css" rel="stylesheet" />
        <div class="wrap">
            <h2>Add New Timeline</h2>
            <?php if (isset($_POST['insert'])) {?>
                <?php if (isset($message)): ?><div class="updated"><p><?php echo $message; ?></p></div><?php endif; ?>
                <a href="<?php echo admin_url('admin.php?page=chronos_timeline_list') ?>">&laquo; Back to timeline list</a>
            <?php } else { ?>
                <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>" enctype="multipart/form-data">
                    <p>Three capital letters for the ID</p>
                    <table class='wp-list-table widefat fixed'>
                        <tr>
                            <th class="ss-th-width">Titre</th>
                            <td><input type="text" name="timeline_title" id="timeline_title" value="<?php echo $title; ?>" class="ss-field-width" /></td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Image</th>
                            <td><input type="file" accept=".png,.jpg,.jpeg" name="timeline_image" id="timeline_image" value="<?php echo $image_link; ?>" class="ss-field-width" /></td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Description</th>
                            <td><input type="text" name="timeline_description" id="timeline_description" value="<?php echo $description; ?>" class="ss-field-width" /></td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Lien</th>
                            <td><input type="text" name="timeline_link" id="timeline_link" value="#" class="ss-field-width" /></td>
                            <p>mettre un # s'il n'y a pas de lien</p>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Date</th>
                            <td><input type="date" name="timeline_date" id="timeline_date" value="<?php echo $date; ?>" class="ss-field-width" /></td>
                        </tr>
                    </table>
                    <input type='submit' name="insert" value='Save' class='button'>
                </form>
            <?php } ?>
        </div>
        <?php
    }

    function chronos_timeline_update() {
        global $wpdb;
        $table_name = $wpdb->prefix . "chronos_timeline";
        $id = $_GET["id"];
        $title = $_POST["timeline_title"];
        $image_link = $_POST['timeline_image'];
        $description = $_POST["timeline_description"];
        $link = $_POST["timeline_link"];
        $date = $_POST["timeline_date"];
    //$_POST
        if (isset($_POST['update'])) {
            $wpdb->update(
                $table_name, //table
                array('title' => $title,
                    'image' => $image_link,
                    'description' => $description,
                    'link' => $link,
                    'timeline_date' => $date
                ), //data
                array('ID' => $id), //where
                array('%s', '%s'), //data format
                array('%s') //where format
            );
        }
    //delete
        else if (isset($_POST['delete'])) {
            $wpdb->query($wpdb->prepare("DELETE FROM $table_name WHERE id = %s", $id));
        } else {//selecting value to update
            $timelines = $wpdb->get_results($wpdb->prepare("SELECT * from $table_name where id=%s", $id));
            foreach ($timelines as $t) {
                $title = $t->title;
                $image_link = $t->image;
                $description = $t->description;
                $link = $t->link;
                $date = $t->timeline_date;
            }
        };
        ?>
        <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/functions/chronos-form.css" rel="stylesheet" />
        <div class="wrap">
            <h2>Schools</h2>

            <?php if ($_POST['delete']) { ?>
                <div class="updated"><p>Timeline supprimé</p></div>
                <a href="<?php echo admin_url('admin.php?page=chronos_timeline_list') ?>"> Retournez à la liste</a>

            <?php } else if ($_POST['update']) { ?>
                <div class="updated"><p>Timeline mise à jour</p></div>
                <a href="<?php echo admin_url('admin.php?page=chronos_timeline_list') ?>"> Retournez à la liste</a>

            <?php } else { ?>
                <form method="post" action="<?php echo $_SERVER['REQUEST_URI']; ?>">
                    <table class='wp-list-table widefat fixed'>
                        <tr>
                            <th class="ss-th-width">Titre</th>
                            <td><input type="text" name="timeline_title" id="timeline_title" value="<?= $title; ?>" class="ss-field-width" /></td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Image</th>
                            <td>
                                <img src="<?= $image_link?>" alt="image">
                                <input type="file" accept=".png,.jpg,.jpeg" name="timeline_image" id="timeline_image" value="<?= $image_link; ?>" class="ss-field-width" />
                            </td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Description</th>
                            <td><input type="text" name="timeline_description" id="timeline_description" value="<?= $description; ?>" class="ss-field-width" /></td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Lien</th>
                            <td><input type="text" name="timeline_link" id="timeline_link" value="<?= $link; ?>" class="ss-field-width" /></td>
                        </tr>
                        <tr>
                            <th class="ss-th-width">Date</th>
                            <td><input type="date" name="timeline_date" id="timeline_date" value="<?= $date; ?>" class="ss-field-width" /></td>
                        </tr>
                    </table>
                    <input type='submit' name="update" value='Save' class='button'> &nbsp;&nbsp;
                    <input type='submit' name="delete" value='Delete' class='button' onclick="return confirm('Etes-vous sur de supprimer ce timeline?')">
                </form>
            <?php } ?>

        </div>
        <?php
    }

    function chronos_timeline_list() {
        ?>
        <div class="wrap">
            <h2>Schools</h2>
            <div class="tablenav top">
                <div class="alignleft actions">
                    <a href="<?php echo admin_url('admin.php?page=chronos_timeline_create'); ?>">Add New</a>
                </div>
                <br class="clear">
            </div>
            <?php
            global $wpdb;
            $table_name = $wpdb->prefix . "chronos_timeline";

            $rows = $wpdb->get_results("SELECT * from $table_name");
            ?>
            <table class='wp-list-table widefat fixed striped posts'>
                <tr>
                    <th class="manage-column ss-list-width">ID</th>
                    <th class="manage-column ss-list-width">Titre</th>
                    <th class="manage-column ss-list-width">Image</th>
                    <th class="manage-column ss-list-width">Description</th>
                    <th class="manage-column ss-list-width">Lien</th>
                    <th class="manage-column ss-list-width">Date</th>
                    <th class="manage-column ss-list-width">Crée le</th>
                    <th class="manage-column ss-list-width">Mise à jour le</th>
                    <th>&nbsp;</th>
                </tr>
                <?php foreach ($rows as $row) { ?>
                    <tr>
                        <td class="manage-column ss-list-width"><?php echo $row->id; ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->title; ?></td>
                        <td class="manage-column ss-list-width"><?php echo mb_strimwidth($row->image, 0, 50, '...'); ?></td>
                        <td class="manage-column ss-list-width"><?php echo mb_strimwidth($row->description, 0, 100, '...'); ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->link; ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->timeline_date; ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->timeline_add; ?></td>
                        <td class="manage-column ss-list-width"><?php echo $row->timeline_edit; ?></td>
                        <td><a href="<?php echo admin_url('admin.php?page=chronos_timeline_update&id=' . $row->id); ?>">Mettre à jour</a></td>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <?php
    }
}

new Chronos_Plugin();