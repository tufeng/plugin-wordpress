<?php

class Chronos_Timeline_Widget extends WP_Widget
{
    public function __construct()
    {
        parent::__construct('chronos_timeline', 'Timeline', array('description' => 'affichage de timeline'));
    }

    public function widget($atts, $content)
    {

        global $wpdb;
        $table_name = $wpdb->prefix . "chronos_timeline";

        $rows = $wpdb->get_results("SELECT * from $table_name ORDER BY timeline_date DESC");
        ?>
        <link type="text/css" href="<?php echo WP_PLUGIN_URL; ?>/Chronos/functions/assets/styles/chronos-timeline-style.css" rel="stylesheet" />
        <div class="main-container">
            <section id="timeline" class="timeline-outer">
                <div class="container" id="content">
                    <div class="row">
                        <div class="col s12 m12 l12">
                            <ul class="timeline">
                                <?php foreach ($rows as $row) { ?>
                                    <li class="event" data-date="<?= $row->timeline_date ?>">
                                        <a href="<?= $row->link ?>" class="timeline-link">
                                            <img src="<?= $row->image ?>" class="timeline-image">
                                            <h3 class="timeline-title"><?= $row->title ?></h3>
                                            <p class="timeline-description">
                                                <?= $row->description ?>
                                            </p>
                                        </a>
                                    </li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <?php
    }
}