<?php
/* 
* Plugin Name: Plugin Timeline
* Description: This is a plugin which display a timeline
* Author: Antoine Lin, Caroline Lecavelier, Thibault Duval, Sabrina Si Hadj Mohand
* Author URI: #
* Version: 0.0
*/ 

require_once plugin_dir_path(__FILE__).'/functions/chronos.php';

register_activation_hook(__FILE__, 'install');
//register_deactivation_hook(__FILE__, 'uninstall');
register_uninstall_hook(__FILE__, 'uninstall');

function install()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "chronos_timeline";
    $charset_collate = $wpdb->get_charset_collate();

    $sql = "CREATE TABLE IF NOT EXISTS $table_name (
                                id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
                                title VARCHAR(255) CHARACTER SET utf8 NOT NULL, 
                                image VARCHAR(255) CHARACTER SET utf8 NOT NULL,
                                description VARCHAR(255) CHARACTER SET utf8 NOT NULL,
                                link TEXT CHARACTER SET utf8 NOT NULL, 
                                timeline_date DATE NOT NULL,
                                timeline_add TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
                                timeline_edit TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
                                ) $charset_collate;";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}

function uninstall()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "chronos_timeline";

    $sql = "DROP TABLE IF EXISTS $table_name";

    require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
    dbDelta( $sql );
}